<?php
/**
 * Plugin Name:     ValMedia Jobs Feed
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     PLUGIN DESCRIPTION HERE
 * Author:          Val Media
 * Author URI:      YOUR SITE HERE
 * Text Domain:     valmedia-jobs-feed
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Valzyb_Jobs_Feed
 */

add_shortcode( 'valmedia_jobs_feed', 'valmedia_jobs_feed' );

function valmedia_jobs_feed( $args ) {
	include_once ABSPATH . WPINC . '/feed.php';

	// @todo Move the Feed URL and API KEY to a settings page.
	$feed_url = defined( 'VALMEDIA_JOBS_FEED_URL' ) ? VALMEDIA_JOBS_FEED_URL : '';
	$api_key  = defined( 'VALMEDIA_JOBS_API_KEY' ) ? VALMEDIA_JOBS_API_KEY : '';

	// Allow parameter override within the shortcode.
	// @todo Use natural names for attributes, not things like 'noofjobs'.
	$feed_params = wp_parse_args( $args, array(
		'a'            => $api_key,
		'noofjobs'     => 15,
		'states'       => '',
		'zip'          => '',
		'miles'        => 0,
		'country'      => 'US',
		'divisions'    => '',
		'SearchString' => '',
	) );
	$feed_url    = add_query_arg( $feed_params, $feed_url );
	// @todo Cache the results of fetch_feed and use them first.
	$rss = fetch_feed( $feed_url );

	if ( is_wp_error( $rss ) ) {
		wp_die( esc_html__( 'There was a problem with this RSS feed.', 'valmedia-jobs-feed' ) );
	}

	// Figure out how many total items there are, but limit it to the numbers set.
	$items = $rss->get_item_quantity( $feed_params['noofjobs'] );

	// Build an array of all the items, starting with element 0 (first element).
	$rss_items = $rss->get_items( 0, $items );

	?>

	<ul>
		<?php if ( 0 === $feed_params['noofjobs'] ) : ?>
			<li><?php _e( 'No jobs listed.', 'valmedia-jobs-feed' ); ?></li>
		<?php else : ?>
			<?php foreach ( $rss_items as $item ) : ?>
				<li>
					<a href="<?php echo esc_url( $item->get_permalink() ); ?>"
					   title="<?php printf( __( 'Posted %s', 'valmedia-jobs-feed' ), $item->get_date( 'j F Y | g:i a' ) ); ?>">
						<?php echo esc_html( $item->get_title() ); ?>
					</a>
				</li>
			<?php endforeach; ?>
		<?php endif; ?>
	</ul>
	<?php
}
